#!/usr/bin/python3
"""Try to implement a reduce() that can stop."""

import functools
import itertools

from typing import Any, Callable, Iterable, NamedTuple, TypeVar  # noqa: H301


_TState = TypeVar("_TState")
_TValue = TypeVar("_TValue")


class ReduceStop(Exception):
    """Not an error, but a signal to stop iterating."""

    result: Any

    def __init__(self, result: Any) -> None:
        """Store the result."""
        super().__init__("We should never see this")
        self.result = result


def bounded_reduce(
    func: Callable[[_TState, _TValue], _TState],
    iterable: Iterable[_TValue],
    initial: _TState,
) -> _TState:
    """Iterate, call func(), stop when it raises a specific exception."""
    try:
        return functools.reduce(func, iterable, initial)
    except ReduceStop as not_err:
        result = not_err.result
        if not isinstance(result, type(initial)):
            raise
        return result


class State(NamedTuple):
    """Something to hold things in."""

    value: int


def process(state: State, value: int) -> State:
    """Process something or signal a stop."""
    result = State(value=state.value + value)
    if result.value > 3048:
        print("process() signalling the end of the world")
        raise ReduceStop(result)
    return result


def process_str(state: State, value: str) -> State:
    """Process a character or signal a stop."""
    result = State(value=state.value + ord(value[0]))
    if result.value > 3600:
        print("process() signalling the end of the world")
        raise ReduceStop(result)
    return result


def main() -> None:
    """Use the bounded_reduce() implementation."""
    for steps in [3, 100]:
        print()
        print(f"Running {steps} steps for int")
        print(
            repr(
                bounded_reduce(
                    process,
                    itertools.islice(itertools.count(step=8), steps),
                    State(value=3000),
                )
            )
        )

        print()
        print(f"Running {steps} steps for str")
        print(
            repr(
                bounded_reduce(
                    process_str,
                    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[:steps],
                    State(value=3000),
                )
            )
        )


if __name__ == "__main__":
    main()
